import { render } from '@stencil/core/testing';
import { DevelopMent } from './develop-ment';

describe('develop-ment', () => {
  it('should build', () => {
    expect(new DevelopMent()).toBeTruthy();
  });

  describe('rendering', () => {
    let element;
    beforeEach(async () => {
      element = await render({
        components: [DevelopMent],
        html: '<develop-ment></develop-ment>'
      });
    });

    it('should render the logo brackets', () => {
        expect(element.querySelector('logo-brackets')).not.toBeNull();
    });

    it('should render a knockout letter \'d\'', () => {
      const letter = element.querySelector('knockout-letter');
      expect(letter).not.toBeNull();
      expect(letter.attributes.getNamedItem('letter').value).toBe('d');
    });

    it('should render a title', () => {
      expect(element.querySelector('.title')).not.toBeNull();
    });

    it('should render copy text', () => {
        expect(element.querySelector('.copy')).not.toBeNull();
    });
});
});