import { Component } from '@stencil/core';

@Component({
  tag: 'develop-ment',
  styleUrl: 'develop-ment.scss'
})
/**
 * The DevelopMent component renders information about my development endeavors. It is CamelCased incorrectly because
 * we need a hyphen in our component tag (e.g. develop-ment).
 */
export class DevelopMent {
  render() {
    return (
        <section>
          <logo-brackets />
          <div class="body">
            <div class="information">
              <div class="title">develop<br/>ment</div>
              <p class="copy">
                JavaScript is my language of choice. I love how the ecosystem has matured over the years. From jQuery 
                to the first frameworks. Angular and React changing the game. I'm curious to see where we'll go next. 
                Web components for sure, but in which exact shape only time can tell.
              </p>
              <p class="copy">
                I love creating (micro) services with Node.js. Combined with Docker Node.js gives you a lot of 
                flexibility in how you want to organize your services. Continuous Deployment becomes a breeze once 
                you've setup your tests, Dockerfiles and build pipelines correctly.
              </p>
            </div>
            <knockout-letter letter="d"></knockout-letter>
          </div>
        </section>
      );
    }
}
