import { render } from '@stencil/core/testing';
import { WelcomeScreen } from './welcome-screen';

describe('develop-ment', () => {
  it('should build', () => {
    expect(new WelcomeScreen()).toBeTruthy();
  });

  describe('rendering', () => {
    let element;
    beforeEach(async () => {
      element = await render({
        components: [WelcomeScreen],
        html: '<welcome-screen></welcome-screen>'
      });
    });

    it('should render the logo', () => {
      expect(element.querySelector('.logo')).not.toBeNull();
      expect(element.querySelector('logo-brackets')).not.toBeNull();
      expect(element.querySelector('logo-name')).not.toBeNull();
    });

    it('should render the navigation', () => {
      expect(element.querySelector('nav')).not.toBeNull();
    })
  });
});