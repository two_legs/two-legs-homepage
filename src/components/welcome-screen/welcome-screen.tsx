import { Component } from '@stencil/core';

@Component({
  tag: 'welcome-screen',
  styleUrl: 'welcome-screen.scss'
})
/**
 * The DevelopMent component renders information about my development endeavors. It is CamelCased incorrectly because
 * we need a hyphen in our component tag (e.g. develop-ment).
 */
export class WelcomeScreen {
  render() {
    return (
      <section>
        <div class="logo">
          <logo-brackets></logo-brackets>
          <logo-name></logo-name>
        </div>
        <nav class="welcome-navigation">
          <a href="#development" class="welcome-link development-link">development</a>
          <span class="divider" />
          <a href="#agile-coaching" class="welcome-link">agile coaching</a>
        </nav>
      </section>
    );
  }
}