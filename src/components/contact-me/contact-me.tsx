import { Component } from '@stencil/core';


@Component({
  tag: 'contact-me',
  styleUrl: 'contact-me.scss'
})
export class ContactMe {
  render() {
    return ([
        <logo-brackets></logo-brackets>,
        <section class="body">
            <div class="information">
                <h2 class="title">Contact</h2>
                <p class="copy">
                    <a class="contact-point" href="mailto:rob@two-legs.com">
                        <icon-mail class="icon"></icon-mail> rob@two-legs.com
                    </a>
                    <span class="contact-point">
                        <icon-mobile class="icon"></icon-mobile> +31 (0)6 141 55 932
                    </span>
                    <a class="contact-point"  href="https://nl.linkedin.com/in/rmbeen" target="_blank">
                        <icon-linkedin class="icon"></icon-linkedin> Linkedin
                    </a>
                </p>
            </div>
            <knockout-letter letter="c"></knockout-letter>
            <nav class="footer">
                <span class="footer-title">Rob Been</span>
                <span class="footer-divider"> } </span> 
                <a class="footer-link" href="#development">Developer</a>
                <span class="footer-divider"> } </span> 
                <a class="footer-link" href="#agile-coaching">Agile Coach</a>
            </nav>
        </section>
    ]);
  }
}
