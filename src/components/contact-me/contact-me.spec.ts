import { render } from '@stencil/core/testing';
import { ContactMe } from './contact-me';

describe('contact-me', () => {
  it('should build', () => {
    expect(new ContactMe()).toBeTruthy();
  });

  describe('rendering', () => {
    let element;
    beforeEach(async () => {
      element = await render({
        components: [ContactMe],
        html: '<contact-me></contact-me>'
      });
    });

    it('should render the logo brackets', () => {
      expect(element.querySelector('logo-brackets')).not.toBeNull();
    });

    it('should render a knockout letter \'c\'', () => {
      const letter = element.querySelector('knockout-letter');
      expect(letter).not.toBeNull();
      expect(letter.attributes.getNamedItem('letter').value).toBe('c');
    });

    it('should render a title', () => {
      expect(element.querySelector('.title')).not.toBeNull();
    });

    it('should render copy', () => {
      expect(element.querySelector('.copy')).not.toBeNull();
    });

    it('should render a footer', () => {
      expect(element.querySelector('.footer')).not.toBeNull();
    });
  });
});
