import { render } from '@stencil/core/testing';
import { AgileCoaching } from './agile-coaching';

describe('agile-coaching', () => {
  it('should build', () => {
    expect(new AgileCoaching()).toBeTruthy();
  });

  describe('rendering', () => {
    let element;
    beforeEach(async () => {
      element = await render({
        components: [AgileCoaching],
        html: '<agile-coaching></agile-coaching>'
      });
    });

    it('should render the logo brackets', () => {
        expect(element.querySelector('logo-brackets')).not.toBeNull();
    });

    it('should render a knockout letter \'a\'', () => {
        const letter = element.querySelector('knockout-letter');
        expect(letter).not.toBeNull();
        expect(letter.attributes.getNamedItem('letter').value).toBe('a');
    });

    it('should render a title', () => {
        expect(element.querySelector('.title')).not.toBeNull();
    });

    it('should render copy text', () => {
        expect(element.querySelector('.copy')).not.toBeNull();
    });
  });
});
