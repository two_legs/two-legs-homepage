import { Component } from '@stencil/core';

@Component({
  tag: 'agile-coaching',
  styleUrl: 'agile-coaching.scss'
})
/**
 * The AgileCoaching component renders information about my Agile Coaching endeavors. 
 */
export class AgileCoaching {
  render() {
    return (
        <section>
          <logo-brackets />
          <div class="body">
            <knockout-letter letter="a"></knockout-letter>
            <div class="information">
              <h2 class="title">agile<br />coaching</h2>
              <p class="copy">
                I have worked in many Agile teams. In those teams I would naturally gravitate
                towards the Scrum master role. I liked coaching the teams so much that I decided to take this one step
                further. In the summer of 2017 I started my first Agile Coaching assignment.
              </p>
              <p class="copy">
                What I like about Agile is how it creates focus for everybody involved. By creating our products
                iteratively we create tight feedback loops on all levels. Not only in the teams, but also for 
                stakeholders and the people who use our products.
              </p>
            </div>
          </div>
        </section>
    );
  }
}
