import { flush, render } from '@stencil/core/testing';
import { KnockoutLetter } from './knockout-letter';

describe('knockout-letter', () => {
  it('should build', () => {
    expect(new KnockoutLetter()).toBeTruthy();
  });

  describe('rendering', () => {
    let element;
    beforeEach(async () => {
      element = await render({
        components: [KnockoutLetter],
        html: '<knockout-letter></knockout-letter>'
      });
    });

    it('should be empty without parameters', () => {
        expect(element.textContent).toEqual('');
    });

    it('should render the provided letter', async () => {
        element.letter = 'A';
        await flush(element);
        expect(element.textContent).toEqual('A');

        element.letter = 'a';
        await flush(element);
        expect(element.textContent).toEqual('a');
    });

    it('should set the class for the provided letter', async () => {
      element.letter = 'A';
      await flush(element);
      expect(element.querySelector('span').classList).toContain('letter-a');
    });
  });
});