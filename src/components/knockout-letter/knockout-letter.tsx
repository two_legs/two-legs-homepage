import { Component, Prop } from '@stencil/core';


@Component({
  tag: 'knockout-letter',
  styleUrl: 'knockout-letter.scss'
})
/**
 * KnockoutLetter gives us a Component which has a so called knockout background. So the fill color of the
 * letter is replaced by the background specified in the knockout-letter.scss file. For now it will always
 * be the same background applied to the letter.
 * 
 * For more information @see https://css-tricks.com/how-to-do-knockout-text/
 */
export class KnockoutLetter {
  /**
   * The letter to be rendered.
   */
  @Prop() letter: string;

  render() {
    const className = this.letter ? `knockout-letter letter-${this.letter.toLowerCase()}`: 'knockout-letter';
    return (
        <span class={className}>{this.letter}</span>
    );
  }
}
