import { Component } from '@stencil/core';


@Component({
  tag: 'logo-brackets',
  styleUrl: 'logo-brackets.scss'
})
export class LogoBrackets {
  render() {
    return (
      
  <svg viewBox="-70 -70 600 600" xmlns="http://www.w3.org/2000/svg" xmlnsXlink="http://www.w3.org/1999/xlink">
  <defs>
    <path d="M234.88,470.2 C122.958522,470.197377 26.6246904,391.134605 4.79227741,281.363192 C-17.0401355,171.591779 41.7094811,61.6844928 145.112301,18.8560658 C248.515121,-23.9723611 367.772113,12.2063455 429.950653,105.266791 C492.129193,198.327236 479.911748,322.350875 400.77,401.49 C356.868867,445.629205 297.133914,470.370833 234.88,470.2 L234.88,470.2 Z M234.88,15.06 C129.665515,15.0739988 39.1129134,89.4092393 18.6009642,192.604918 C-1.91098496,295.800597 53.3306381,399.114993 150.542011,439.364678 C247.753384,479.614363 359.859516,445.589126 418.300262,358.097599 C476.741009,270.606072 465.239646,154.016106 390.83,79.63 C349.557545,38.1398956 293.401938,14.8854611 234.88,15.05 L234.88,15.06 Z" id="logo-brackets-path-1"></path>
    <filter x="-20.0%" y="-6.8%" width="140.1%" height="140.1%" filterUnits="objectBoundingBox" id="logo-brackets-filter-2">
        <feOffset dx="0" dy="2" in="SourceAlpha" result="shadowOffsetOuter1"></feOffset>
        <feGaussianBlur stdDeviation="1" in="shadowOffsetOuter1" result="shadowBlurOuter1"></feGaussianBlur>
        <feColorMatrix values="0 0 0 0 0   0 0 0 0 0   0 0 0 0 0  0 0 0 0.2 0" type="matrix" in="shadowBlurOuter1" result="shadowMatrixOuter1"></feColorMatrix>
        <feOffset dx="0" dy="4" in="SourceAlpha" result="shadowOffsetOuter2"></feOffset>
        <feGaussianBlur stdDeviation="2" in="shadowOffsetOuter2" result="shadowBlurOuter2"></feGaussianBlur>
        <feColorMatrix values="0 0 0 0 0   0 0 0 0 0   0 0 0 0 0  0 0 0 0.2 0" type="matrix" in="shadowBlurOuter2" result="shadowMatrixOuter2"></feColorMatrix>
        <feOffset dx="0" dy="8" in="SourceAlpha" result="shadowOffsetOuter3"></feOffset>
        <feGaussianBlur stdDeviation="4" in="shadowOffsetOuter3" result="shadowBlurOuter3"></feGaussianBlur>
        <feColorMatrix values="0 0 0 0 0   0 0 0 0 0   0 0 0 0 0  0 0 0 0.2 0" type="matrix" in="shadowBlurOuter3" result="shadowMatrixOuter3"></feColorMatrix>
        <feOffset dx="0" dy="16" in="SourceAlpha" result="shadowOffsetOuter4"></feOffset>
        <feGaussianBlur stdDeviation="8" in="shadowOffsetOuter4" result="shadowBlurOuter4"></feGaussianBlur>
        <feColorMatrix values="0 0 0 0 0   0 0 0 0 0   0 0 0 0 0  0 0 0 0.2 0" type="matrix" in="shadowBlurOuter4" result="shadowMatrixOuter4"></feColorMatrix>
        <feOffset dx="0" dy="32" in="SourceAlpha" result="shadowOffsetOuter5"></feOffset>
        <feGaussianBlur stdDeviation="16" in="shadowOffsetOuter5" result="shadowBlurOuter5"></feGaussianBlur>
        <feColorMatrix values="0 0 0 0 0   0 0 0 0 0   0 0 0 0 0  0 0 0 0.1 0" type="matrix" in="shadowBlurOuter5" result="shadowMatrixOuter5"></feColorMatrix>
        <feMerge>
            <feMergeNode in="shadowMatrixOuter1"></feMergeNode>
            <feMergeNode in="shadowMatrixOuter2"></feMergeNode>
            <feMergeNode in="shadowMatrixOuter3"></feMergeNode>
            <feMergeNode in="shadowMatrixOuter4"></feMergeNode>
            <feMergeNode in="shadowMatrixOuter5"></feMergeNode>
        </feMerge>
    </filter>
    <path d="M161.6,312.42 C161.6,320.726667 164.023333,326.936667 168.87,331.05 C173.716667,335.163333 181.96,337.356667 193.6,337.63 L193.6,348.74 C177.72,348.74 166.423333,345.686667 159.71,339.58 C152.996667,333.473333 149.636667,324.07 149.63,311.37 L149.63,270.16 C149.63,253.493333 140.266667,244.983333 121.54,244.63 L121.54,234.86 C131.473333,234.7 138.64,232.623333 143.04,228.63 C147.44,224.636667 149.64,218.61 149.64,210.55 L149.64,167.81 C149.64,143.463333 164.306667,131.046667 193.64,130.56 L193.64,141.56 C182.4,141.966667 174.256667,144.326667 169.21,148.64 C164.163333,152.953333 161.64,159.346667 161.64,167.82 L161.64,205.07 C161.64,215.656667 159.85,223.556667 156.27,228.77 C152.69,233.983333 146.91,237.443333 138.93,239.15 L138.93,240.15 C146.83,241.776667 152.59,245.176667 156.21,250.35 C159.83,255.523333 161.643333,263.116667 161.65,273.13 L161.65,312.46 L161.6,312.42 Z" id="logo-brackets-path-3"></path>
    <filter x="-130.4%" y="-14.7%" width="360.7%" height="186.2%" filterUnits="objectBoundingBox" id="logo-brackets-filter-4">
        <feOffset dx="0" dy="2" in="SourceAlpha" result="shadowOffsetOuter1"></feOffset>
        <feGaussianBlur stdDeviation="1" in="shadowOffsetOuter1" result="shadowBlurOuter1"></feGaussianBlur>
        <feColorMatrix values="0 0 0 0 0   0 0 0 0 0   0 0 0 0 0  0 0 0 0.2 0" type="matrix" in="shadowBlurOuter1" result="shadowMatrixOuter1"></feColorMatrix>
        <feOffset dx="0" dy="4" in="SourceAlpha" result="shadowOffsetOuter2"></feOffset>
        <feGaussianBlur stdDeviation="2" in="shadowOffsetOuter2" result="shadowBlurOuter2"></feGaussianBlur>
        <feColorMatrix values="0 0 0 0 0   0 0 0 0 0   0 0 0 0 0  0 0 0 0.2 0" type="matrix" in="shadowBlurOuter2" result="shadowMatrixOuter2"></feColorMatrix>
        <feOffset dx="0" dy="8" in="SourceAlpha" result="shadowOffsetOuter3"></feOffset>
        <feGaussianBlur stdDeviation="4" in="shadowOffsetOuter3" result="shadowBlurOuter3"></feGaussianBlur>
        <feColorMatrix values="0 0 0 0 0   0 0 0 0 0   0 0 0 0 0  0 0 0 0.2 0" type="matrix" in="shadowBlurOuter3" result="shadowMatrixOuter3"></feColorMatrix>
        <feOffset dx="0" dy="16" in="SourceAlpha" result="shadowOffsetOuter4"></feOffset>
        <feGaussianBlur stdDeviation="8" in="shadowOffsetOuter4" result="shadowBlurOuter4"></feGaussianBlur>
        <feColorMatrix values="0 0 0 0 0   0 0 0 0 0   0 0 0 0 0  0 0 0 0.2 0" type="matrix" in="shadowBlurOuter4" result="shadowMatrixOuter4"></feColorMatrix>
        <feOffset dx="0" dy="32" in="SourceAlpha" result="shadowOffsetOuter5"></feOffset>
        <feGaussianBlur stdDeviation="16" in="shadowOffsetOuter5" result="shadowBlurOuter5"></feGaussianBlur>
        <feColorMatrix values="0 0 0 0 0   0 0 0 0 0   0 0 0 0 0  0 0 0 0.1 0" type="matrix" in="shadowBlurOuter5" result="shadowMatrixOuter5"></feColorMatrix>
        <feMerge>
            <feMergeNode in="shadowMatrixOuter1"></feMergeNode>
            <feMergeNode in="shadowMatrixOuter2"></feMergeNode>
            <feMergeNode in="shadowMatrixOuter3"></feMergeNode>
            <feMergeNode in="shadowMatrixOuter4"></feMergeNode>
            <feMergeNode in="shadowMatrixOuter5"></feMergeNode>
        </feMerge>
    </filter>
    <path d="M308.17,273.09 C308.17,263.09 309.983333,255.496667 313.61,250.31 C317.236667,245.123333 322.996667,241.723333 330.89,240.11 L330.89,239.11 C322.99,237.483333 317.23,234.063333 313.61,228.85 C309.99,223.636667 308.176667,215.696667 308.17,205.03 L308.17,167.81 C308.17,159.423333 305.666667,153.05 300.66,148.69 C295.653333,144.33 287.49,141.946667 276.17,141.54 L276.17,130.54 C290.336667,130.54 301.206667,133.696667 308.78,140.01 C316.353333,146.323333 320.14,155.586667 320.14,167.8 L320.14,210.55 C320.14,218.696667 322.36,224.743333 326.8,228.69 C331.24,232.636667 338.386667,234.693333 348.24,234.86 L348.24,244.63 C329.513333,244.956667 320.15,253.466667 320.15,270.16 L320.15,311.32 C320.15,323.94 316.79,333.323333 310.07,339.47 C303.35,345.616667 292.053333,348.69 276.18,348.69 L276.18,337.63 C287.66,337.463333 295.863333,335.326667 300.79,331.22 C305.716667,327.113333 308.18,320.863333 308.18,312.47 L308.18,273.09 L308.17,273.09 Z" id="logo-brackets-path-5"></path>
    <filter x="-130.4%" y="-14.7%" width="360.9%" height="186.2%" filterUnits="objectBoundingBox" id="logo-brackets-filter-6">
        <feOffset dx="0" dy="2" in="SourceAlpha" result="shadowOffsetOuter1"></feOffset>
        <feGaussianBlur stdDeviation="1" in="shadowOffsetOuter1" result="shadowBlurOuter1"></feGaussianBlur>
        <feColorMatrix values="0 0 0 0 0   0 0 0 0 0   0 0 0 0 0  0 0 0 0.2 0" type="matrix" in="shadowBlurOuter1" result="shadowMatrixOuter1"></feColorMatrix>
        <feOffset dx="0" dy="4" in="SourceAlpha" result="shadowOffsetOuter2"></feOffset>
        <feGaussianBlur stdDeviation="2" in="shadowOffsetOuter2" result="shadowBlurOuter2"></feGaussianBlur>
        <feColorMatrix values="0 0 0 0 0   0 0 0 0 0   0 0 0 0 0  0 0 0 0.2 0" type="matrix" in="shadowBlurOuter2" result="shadowMatrixOuter2"></feColorMatrix>
        <feOffset dx="0" dy="8" in="SourceAlpha" result="shadowOffsetOuter3"></feOffset>
        <feGaussianBlur stdDeviation="4" in="shadowOffsetOuter3" result="shadowBlurOuter3"></feGaussianBlur>
        <feColorMatrix values="0 0 0 0 0   0 0 0 0 0   0 0 0 0 0  0 0 0 0.2 0" type="matrix" in="shadowBlurOuter3" result="shadowMatrixOuter3"></feColorMatrix>
        <feOffset dx="0" dy="16" in="SourceAlpha" result="shadowOffsetOuter4"></feOffset>
        <feGaussianBlur stdDeviation="8" in="shadowOffsetOuter4" result="shadowBlurOuter4"></feGaussianBlur>
        <feColorMatrix values="0 0 0 0 0   0 0 0 0 0   0 0 0 0 0  0 0 0 0.2 0" type="matrix" in="shadowBlurOuter4" result="shadowMatrixOuter4"></feColorMatrix>
        <feOffset dx="0" dy="32" in="SourceAlpha" result="shadowOffsetOuter5"></feOffset>
        <feGaussianBlur stdDeviation="16" in="shadowOffsetOuter5" result="shadowBlurOuter5"></feGaussianBlur>
        <feColorMatrix values="0 0 0 0 0   0 0 0 0 0   0 0 0 0 0  0 0 0 0.1 0" type="matrix" in="shadowBlurOuter5" result="shadowMatrixOuter5"></feColorMatrix>
        <feMerge>
            <feMergeNode in="shadowMatrixOuter1"></feMergeNode>
            <feMergeNode in="shadowMatrixOuter2"></feMergeNode>
            <feMergeNode in="shadowMatrixOuter3"></feMergeNode>
            <feMergeNode in="shadowMatrixOuter4"></feMergeNode>
            <feMergeNode in="shadowMatrixOuter5"></feMergeNode>
        </feMerge>
    </filter>
</defs>
<g id="logo-brackets-Symbols" stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
    <g id="logo-brackets-LOGO_SHAPE" fill-rule="nonzero">
        <g id="logo-brackets-Shape">
            <use fill="black" fill-opacity="1" filter="url(#logo-brackets-filter-2)" xlinkHref="#logo-brackets-path-1"></use>
            <use fill="#FFFFFF" fill-rule="evenodd" xlinkHref="#logo-brackets-path-1"></use>
        </g>
        <g id="logo-brackets-Shape">
            <use fill="black" fill-opacity="1" filter="url(#logo-brackets-filter-4)" xlinkHref="#logo-brackets-path-3"></use>
            <use fill="#FFFFFF" fill-rule="evenodd" xlinkHref="#logo-brackets-path-3"></use>
        </g>
        <g id="logo-brackets-Shape">
            <use fill="black" fill-opacity="1" filter="url(#logo-brackets-filter-6)" xlinkHref="#logo-brackets-path-5"></use>
            <use fill="#FFFFFF" fill-rule="evenodd" xlinkHref="#logo-brackets-path-5"></use>
        </g>
    </g>
</g>
</svg>
    );
  }
}
