import { Component } from '@stencil/core';


@Component({
  tag: 'logo-name',
  styleUrl: 'logo-name.scss'
})
export class LogoName {
  render() {
    return (
       


<svg viewBox="0 0 1140 210" version="1.1" xmlns="http://www.w3.org/2000/svg" xmlnsXlink="http://www.w3.org/1999/xlink">
<defs>
    <polygon id="logo-name-path-1" points="56.19 145.06 46.19 145.06 46.19 12.25 0 12.25 0 2.92 102.38 2.92 102.38 12.25 56.19 12.25 56.19 145.07"></polygon>
    <filter x="-91.8%" y="-22.5%" width="283.6%" height="232.3%" filterUnits="objectBoundingBox" id="logo-name-filter-2">
        <feOffset dx="0" dy="2" in="SourceAlpha" result="shadowOffsetOuter1"></feOffset>
        <feGaussianBlur stdDeviation="1" in="shadowOffsetOuter1" result="shadowBlurOuter1"></feGaussianBlur>
        <feColorMatrix values="0 0 0 0 0   0 0 0 0 0   0 0 0 0 0  0 0 0 0.2 0" type="matrix" in="shadowBlurOuter1" result="shadowMatrixOuter1"></feColorMatrix>
        <feOffset dx="0" dy="4" in="SourceAlpha" result="shadowOffsetOuter2"></feOffset>
        <feGaussianBlur stdDeviation="2" in="shadowOffsetOuter2" result="shadowBlurOuter2"></feGaussianBlur>
        <feColorMatrix values="0 0 0 0 0   0 0 0 0 0   0 0 0 0 0  0 0 0 0.2 0" type="matrix" in="shadowBlurOuter2" result="shadowMatrixOuter2"></feColorMatrix>
        <feOffset dx="0" dy="8" in="SourceAlpha" result="shadowOffsetOuter3"></feOffset>
        <feGaussianBlur stdDeviation="4" in="shadowOffsetOuter3" result="shadowBlurOuter3"></feGaussianBlur>
        <feColorMatrix values="0 0 0 0 0   0 0 0 0 0   0 0 0 0 0  0 0 0 0.2 0" type="matrix" in="shadowBlurOuter3" result="shadowMatrixOuter3"></feColorMatrix>
        <feOffset dx="0" dy="16" in="SourceAlpha" result="shadowOffsetOuter4"></feOffset>
        <feGaussianBlur stdDeviation="8" in="shadowOffsetOuter4" result="shadowBlurOuter4"></feGaussianBlur>
        <feColorMatrix values="0 0 0 0 0   0 0 0 0 0   0 0 0 0 0  0 0 0 0.2 0" type="matrix" in="shadowBlurOuter4" result="shadowMatrixOuter4"></feColorMatrix>
        <feOffset dx="0" dy="32" in="SourceAlpha" result="shadowOffsetOuter5"></feOffset>
        <feGaussianBlur stdDeviation="16" in="shadowOffsetOuter5" result="shadowBlurOuter5"></feGaussianBlur>
        <feColorMatrix values="0 0 0 0 0   0 0 0 0 0   0 0 0 0 0  0 0 0 0.1 0" type="matrix" in="shadowBlurOuter5" result="shadowMatrixOuter5"></feColorMatrix>
        <feMerge>
            <feMergeNode in="shadowMatrixOuter1"></feMergeNode>
            <feMergeNode in="shadowMatrixOuter2"></feMergeNode>
            <feMergeNode in="shadowMatrixOuter3"></feMergeNode>
            <feMergeNode in="shadowMatrixOuter4"></feMergeNode>
            <feMergeNode in="shadowMatrixOuter5"></feMergeNode>
        </feMerge>
    </filter>
    <path d="M256.66,145.06 L248.49,145.06 L217.29,35.39 C214.696667,26.3833333 212.753333,18.9933333 211.46,13.22 C210.42,18.86 208.946667,25.3433333 207.04,32.67 C205.133333,39.9966667 194.68,77.4633333 175.68,145.07 L167.29,145.07 L128.22,2.92 L138.62,2.92 L163.51,94.51 C164.483333,98.2033333 165.39,101.623333 166.23,104.77 C167.07,107.916667 167.83,110.866667 168.51,113.62 C169.176667,116.373333 169.793333,119.04 170.36,121.62 C170.926667,124.2 171.43,126.776667 171.87,129.35 C173.423333,120.536667 176.73,107.15 181.79,89.19 L206.1,2.95 L217.1,2.95 L245.59,101.95 C248.923333,113.356667 251.29,122.56 252.69,129.56 C253.53,124.893333 254.616667,119.933333 255.95,114.68 C257.283333,109.426667 267.283333,72.1866667 285.95,2.96 L295.95,2.96 L256.66,145.06 Z" id="logo-name-path-3"></path>
    <filter x="-56.0%" y="-22.5%" width="212.1%" height="232.3%" filterUnits="objectBoundingBox" id="logo-name-filter-4">
        <feOffset dx="0" dy="2" in="SourceAlpha" result="shadowOffsetOuter1"></feOffset>
        <feGaussianBlur stdDeviation="1" in="shadowOffsetOuter1" result="shadowBlurOuter1"></feGaussianBlur>
        <feColorMatrix values="0 0 0 0 0   0 0 0 0 0   0 0 0 0 0  0 0 0 0.2 0" type="matrix" in="shadowBlurOuter1" result="shadowMatrixOuter1"></feColorMatrix>
        <feOffset dx="0" dy="4" in="SourceAlpha" result="shadowOffsetOuter2"></feOffset>
        <feGaussianBlur stdDeviation="2" in="shadowOffsetOuter2" result="shadowBlurOuter2"></feGaussianBlur>
        <feColorMatrix values="0 0 0 0 0   0 0 0 0 0   0 0 0 0 0  0 0 0 0.2 0" type="matrix" in="shadowBlurOuter2" result="shadowMatrixOuter2"></feColorMatrix>
        <feOffset dx="0" dy="8" in="SourceAlpha" result="shadowOffsetOuter3"></feOffset>
        <feGaussianBlur stdDeviation="4" in="shadowOffsetOuter3" result="shadowBlurOuter3"></feGaussianBlur>
        <feColorMatrix values="0 0 0 0 0   0 0 0 0 0   0 0 0 0 0  0 0 0 0.2 0" type="matrix" in="shadowBlurOuter3" result="shadowMatrixOuter3"></feColorMatrix>
        <feOffset dx="0" dy="16" in="SourceAlpha" result="shadowOffsetOuter4"></feOffset>
        <feGaussianBlur stdDeviation="8" in="shadowOffsetOuter4" result="shadowBlurOuter4"></feGaussianBlur>
        <feColorMatrix values="0 0 0 0 0   0 0 0 0 0   0 0 0 0 0  0 0 0 0.2 0" type="matrix" in="shadowBlurOuter4" result="shadowMatrixOuter4"></feColorMatrix>
        <feOffset dx="0" dy="32" in="SourceAlpha" result="shadowOffsetOuter5"></feOffset>
        <feGaussianBlur stdDeviation="16" in="shadowOffsetOuter5" result="shadowBlurOuter5"></feGaussianBlur>
        <feColorMatrix values="0 0 0 0 0   0 0 0 0 0   0 0 0 0 0  0 0 0 0.1 0" type="matrix" in="shadowBlurOuter5" result="shadowMatrixOuter5"></feColorMatrix>
        <feMerge>
            <feMergeNode in="shadowMatrixOuter1"></feMergeNode>
            <feMergeNode in="shadowMatrixOuter2"></feMergeNode>
            <feMergeNode in="shadowMatrixOuter3"></feMergeNode>
            <feMergeNode in="shadowMatrixOuter4"></feMergeNode>
            <feMergeNode in="shadowMatrixOuter5"></feMergeNode>
        </feMerge>
    </filter>
    <path d="M460.43,73.8 C460.43,96.36 454.79,114.2 443.51,127.32 C432.23,140.44 416.673333,147.003333 396.84,147.01 C377.066667,147.01 361.543333,140.446667 350.27,127.32 C338.996667,114.193333 333.36,96.2966667 333.36,73.63 C333.36,51.01 339.046667,33.2 350.42,20.2 C361.793333,7.2 377.333333,0.676666667 397.04,0.63 C416.873333,0.63 432.396667,7.16 443.61,20.22 C454.823333,33.28 460.43,51.14 460.43,73.8 Z M344.14,73.8 C344.14,94.1533333 348.676667,109.886667 357.75,121 C366.823333,132.113333 379.856667,137.656667 396.85,137.63 C413.963333,137.63 427.04,132.12 436.08,121.1 C445.12,110.08 449.64,94.2966667 449.64,73.75 C449.64,53.27 445.12,37.5666667 436.08,26.64 C427.04,15.7133333 414.04,10.2533333 397.08,10.26 C380.16,10.26 367.116667,15.77 357.95,26.79 C348.783333,37.81 344.183333,53.48 344.15,73.8 L344.14,73.8 Z" id="logo-name-path-5"></path>
    <filter x="-74.0%" y="-21.9%" width="247.9%" height="228.4%" filterUnits="objectBoundingBox" id="logo-name-filter-6">
        <feOffset dx="0" dy="2" in="SourceAlpha" result="shadowOffsetOuter1"></feOffset>
        <feGaussianBlur stdDeviation="1" in="shadowOffsetOuter1" result="shadowBlurOuter1"></feGaussianBlur>
        <feColorMatrix values="0 0 0 0 0   0 0 0 0 0   0 0 0 0 0  0 0 0 0.2 0" type="matrix" in="shadowBlurOuter1" result="shadowMatrixOuter1"></feColorMatrix>
        <feOffset dx="0" dy="4" in="SourceAlpha" result="shadowOffsetOuter2"></feOffset>
        <feGaussianBlur stdDeviation="2" in="shadowOffsetOuter2" result="shadowBlurOuter2"></feGaussianBlur>
        <feColorMatrix values="0 0 0 0 0   0 0 0 0 0   0 0 0 0 0  0 0 0 0.2 0" type="matrix" in="shadowBlurOuter2" result="shadowMatrixOuter2"></feColorMatrix>
        <feOffset dx="0" dy="8" in="SourceAlpha" result="shadowOffsetOuter3"></feOffset>
        <feGaussianBlur stdDeviation="4" in="shadowOffsetOuter3" result="shadowBlurOuter3"></feGaussianBlur>
        <feColorMatrix values="0 0 0 0 0   0 0 0 0 0   0 0 0 0 0  0 0 0 0.2 0" type="matrix" in="shadowBlurOuter3" result="shadowMatrixOuter3"></feColorMatrix>
        <feOffset dx="0" dy="16" in="SourceAlpha" result="shadowOffsetOuter4"></feOffset>
        <feGaussianBlur stdDeviation="8" in="shadowOffsetOuter4" result="shadowBlurOuter4"></feGaussianBlur>
        <feColorMatrix values="0 0 0 0 0   0 0 0 0 0   0 0 0 0 0  0 0 0 0.2 0" type="matrix" in="shadowBlurOuter4" result="shadowMatrixOuter4"></feColorMatrix>
        <feOffset dx="0" dy="32" in="SourceAlpha" result="shadowOffsetOuter5"></feOffset>
        <feGaussianBlur stdDeviation="16" in="shadowOffsetOuter5" result="shadowBlurOuter5"></feGaussianBlur>
        <feColorMatrix values="0 0 0 0 0   0 0 0 0 0   0 0 0 0 0  0 0 0 0.1 0" type="matrix" in="shadowBlurOuter5" result="shadowMatrixOuter5"></feColorMatrix>
        <feMerge>
            <feMergeNode in="shadowMatrixOuter1"></feMergeNode>
            <feMergeNode in="shadowMatrixOuter2"></feMergeNode>
            <feMergeNode in="shadowMatrixOuter3"></feMergeNode>
            <feMergeNode in="shadowMatrixOuter4"></feMergeNode>
            <feMergeNode in="shadowMatrixOuter5"></feMergeNode>
        </feMerge>
    </filter>
    <polygon id="logo-name-path-7" points="584.65 145.06 584.65 2.92 594.57 2.92 594.57 135.73 662.34 135.73 662.34 145.06 584.65 145.06"></polygon>
    <filter x="-121.0%" y="-22.5%" width="342.0%" height="232.3%" filterUnits="objectBoundingBox" id="logo-name-filter-8">
        <feOffset dx="0" dy="2" in="SourceAlpha" result="shadowOffsetOuter1"></feOffset>
        <feGaussianBlur stdDeviation="1" in="shadowOffsetOuter1" result="shadowBlurOuter1"></feGaussianBlur>
        <feColorMatrix values="0 0 0 0 0   0 0 0 0 0   0 0 0 0 0  0 0 0 0.2 0" type="matrix" in="shadowBlurOuter1" result="shadowMatrixOuter1"></feColorMatrix>
        <feOffset dx="0" dy="4" in="SourceAlpha" result="shadowOffsetOuter2"></feOffset>
        <feGaussianBlur stdDeviation="2" in="shadowOffsetOuter2" result="shadowBlurOuter2"></feGaussianBlur>
        <feColorMatrix values="0 0 0 0 0   0 0 0 0 0   0 0 0 0 0  0 0 0 0.2 0" type="matrix" in="shadowBlurOuter2" result="shadowMatrixOuter2"></feColorMatrix>
        <feOffset dx="0" dy="8" in="SourceAlpha" result="shadowOffsetOuter3"></feOffset>
        <feGaussianBlur stdDeviation="4" in="shadowOffsetOuter3" result="shadowBlurOuter3"></feGaussianBlur>
        <feColorMatrix values="0 0 0 0 0   0 0 0 0 0   0 0 0 0 0  0 0 0 0.2 0" type="matrix" in="shadowBlurOuter3" result="shadowMatrixOuter3"></feColorMatrix>
        <feOffset dx="0" dy="16" in="SourceAlpha" result="shadowOffsetOuter4"></feOffset>
        <feGaussianBlur stdDeviation="8" in="shadowOffsetOuter4" result="shadowBlurOuter4"></feGaussianBlur>
        <feColorMatrix values="0 0 0 0 0   0 0 0 0 0   0 0 0 0 0  0 0 0 0.2 0" type="matrix" in="shadowBlurOuter4" result="shadowMatrixOuter4"></feColorMatrix>
        <feOffset dx="0" dy="32" in="SourceAlpha" result="shadowOffsetOuter5"></feOffset>
        <feGaussianBlur stdDeviation="16" in="shadowOffsetOuter5" result="shadowBlurOuter5"></feGaussianBlur>
        <feColorMatrix values="0 0 0 0 0   0 0 0 0 0   0 0 0 0 0  0 0 0 0.1 0" type="matrix" in="shadowBlurOuter5" result="shadowMatrixOuter5"></feColorMatrix>
        <feMerge>
            <feMergeNode in="shadowMatrixOuter1"></feMergeNode>
            <feMergeNode in="shadowMatrixOuter2"></feMergeNode>
            <feMergeNode in="shadowMatrixOuter3"></feMergeNode>
            <feMergeNode in="shadowMatrixOuter4"></feMergeNode>
            <feMergeNode in="shadowMatrixOuter5"></feMergeNode>
        </feMerge>
    </filter>
    <polygon id="logo-name-path-9" points="784.44 145.06 706.75 145.06 706.75 2.92 784.44 2.92 784.44 12.06 716.67 12.06 716.67 65.83 780.67 65.83 780.67 74.97 716.67 74.97 716.67 135.97 784.44 135.97 784.44 145.11"></polygon>
    <filter x="-121.0%" y="-22.5%" width="342.0%" height="232.2%" filterUnits="objectBoundingBox" id="logo-name-filter-10">
        <feOffset dx="0" dy="2" in="SourceAlpha" result="shadowOffsetOuter1"></feOffset>
        <feGaussianBlur stdDeviation="1" in="shadowOffsetOuter1" result="shadowBlurOuter1"></feGaussianBlur>
        <feColorMatrix values="0 0 0 0 0   0 0 0 0 0   0 0 0 0 0  0 0 0 0.2 0" type="matrix" in="shadowBlurOuter1" result="shadowMatrixOuter1"></feColorMatrix>
        <feOffset dx="0" dy="4" in="SourceAlpha" result="shadowOffsetOuter2"></feOffset>
        <feGaussianBlur stdDeviation="2" in="shadowOffsetOuter2" result="shadowBlurOuter2"></feGaussianBlur>
        <feColorMatrix values="0 0 0 0 0   0 0 0 0 0   0 0 0 0 0  0 0 0 0.2 0" type="matrix" in="shadowBlurOuter2" result="shadowMatrixOuter2"></feColorMatrix>
        <feOffset dx="0" dy="8" in="SourceAlpha" result="shadowOffsetOuter3"></feOffset>
        <feGaussianBlur stdDeviation="4" in="shadowOffsetOuter3" result="shadowBlurOuter3"></feGaussianBlur>
        <feColorMatrix values="0 0 0 0 0   0 0 0 0 0   0 0 0 0 0  0 0 0 0.2 0" type="matrix" in="shadowBlurOuter3" result="shadowMatrixOuter3"></feColorMatrix>
        <feOffset dx="0" dy="16" in="SourceAlpha" result="shadowOffsetOuter4"></feOffset>
        <feGaussianBlur stdDeviation="8" in="shadowOffsetOuter4" result="shadowBlurOuter4"></feGaussianBlur>
        <feColorMatrix values="0 0 0 0 0   0 0 0 0 0   0 0 0 0 0  0 0 0 0.2 0" type="matrix" in="shadowBlurOuter4" result="shadowMatrixOuter4"></feColorMatrix>
        <feOffset dx="0" dy="32" in="SourceAlpha" result="shadowOffsetOuter5"></feOffset>
        <feGaussianBlur stdDeviation="16" in="shadowOffsetOuter5" result="shadowBlurOuter5"></feGaussianBlur>
        <feColorMatrix values="0 0 0 0 0   0 0 0 0 0   0 0 0 0 0  0 0 0 0.1 0" type="matrix" in="shadowBlurOuter5" result="shadowMatrixOuter5"></feColorMatrix>
        <feMerge>
            <feMergeNode in="shadowMatrixOuter1"></feMergeNode>
            <feMergeNode in="shadowMatrixOuter2"></feMergeNode>
            <feMergeNode in="shadowMatrixOuter3"></feMergeNode>
            <feMergeNode in="shadowMatrixOuter4"></feMergeNode>
            <feMergeNode in="shadowMatrixOuter5"></feMergeNode>
        </feMerge>
    </filter>
    <path d="M892.44,73.41 L944.85,73.41 L944.85,138.26 C931.11,144.093333 915.65,147.01 898.47,147.01 C876.043333,147.01 858.85,140.676667 846.89,128.01 C834.93,115.343333 828.95,97.4033333 828.95,74.19 C828.95,59.7366667 831.916667,46.92 837.85,35.74 C843.568496,24.7658517 852.424647,15.7426032 863.29,9.82 C874.343333,3.72666667 887.033333,0.68 901.36,0.68 C916.853333,0.68 930.756667,3.53333333 943.07,9.24 L939.07,18.19 C927.095687,12.5639514 914.030135,9.64152833 900.8,9.63 C882.066667,9.63 867.206667,15.4166667 856.22,26.99 C845.233333,38.5633333 839.74,54.17 839.74,73.81 C839.74,95.2033333 844.956667,111.293333 855.39,122.08 C865.823333,132.866667 881.156667,138.263333 901.39,138.27 C914.483333,138.27 925.6,136.423333 934.74,132.73 L934.74,82.73 L892.44,82.73 L892.44,73.4 L892.44,73.41 Z" id="logo-name-path-11"></path>
    <filter x="-81.1%" y="-21.9%" width="262.2%" height="228.5%" filterUnits="objectBoundingBox" id="logo-name-filter-12">
        <feOffset dx="0" dy="2" in="SourceAlpha" result="shadowOffsetOuter1"></feOffset>
        <feGaussianBlur stdDeviation="1" in="shadowOffsetOuter1" result="shadowBlurOuter1"></feGaussianBlur>
        <feColorMatrix values="0 0 0 0 0   0 0 0 0 0   0 0 0 0 0  0 0 0 0.2 0" type="matrix" in="shadowBlurOuter1" result="shadowMatrixOuter1"></feColorMatrix>
        <feOffset dx="0" dy="4" in="SourceAlpha" result="shadowOffsetOuter2"></feOffset>
        <feGaussianBlur stdDeviation="2" in="shadowOffsetOuter2" result="shadowBlurOuter2"></feGaussianBlur>
        <feColorMatrix values="0 0 0 0 0   0 0 0 0 0   0 0 0 0 0  0 0 0 0.2 0" type="matrix" in="shadowBlurOuter2" result="shadowMatrixOuter2"></feColorMatrix>
        <feOffset dx="0" dy="8" in="SourceAlpha" result="shadowOffsetOuter3"></feOffset>
        <feGaussianBlur stdDeviation="4" in="shadowOffsetOuter3" result="shadowBlurOuter3"></feGaussianBlur>
        <feColorMatrix values="0 0 0 0 0   0 0 0 0 0   0 0 0 0 0  0 0 0 0.2 0" type="matrix" in="shadowBlurOuter3" result="shadowMatrixOuter3"></feColorMatrix>
        <feOffset dx="0" dy="16" in="SourceAlpha" result="shadowOffsetOuter4"></feOffset>
        <feGaussianBlur stdDeviation="8" in="shadowOffsetOuter4" result="shadowBlurOuter4"></feGaussianBlur>
        <feColorMatrix values="0 0 0 0 0   0 0 0 0 0   0 0 0 0 0  0 0 0 0.2 0" type="matrix" in="shadowBlurOuter4" result="shadowMatrixOuter4"></feColorMatrix>
        <feOffset dx="0" dy="32" in="SourceAlpha" result="shadowOffsetOuter5"></feOffset>
        <feGaussianBlur stdDeviation="16" in="shadowOffsetOuter5" result="shadowBlurOuter5"></feGaussianBlur>
        <feColorMatrix values="0 0 0 0 0   0 0 0 0 0   0 0 0 0 0  0 0 0 0.1 0" type="matrix" in="shadowBlurOuter5" result="shadowMatrixOuter5"></feColorMatrix>
        <feMerge>
            <feMergeNode in="shadowMatrixOuter1"></feMergeNode>
            <feMergeNode in="shadowMatrixOuter2"></feMergeNode>
            <feMergeNode in="shadowMatrixOuter3"></feMergeNode>
            <feMergeNode in="shadowMatrixOuter4"></feMergeNode>
            <feMergeNode in="shadowMatrixOuter5"></feMergeNode>
        </feMerge>
    </filter>
    <path d="M1078.91,108.41 C1078.91,120.27 1074.55,129.67 1065.83,136.61 C1057.11,143.55 1045.52333,147.016667 1031.07,147.01 C1013.69667,147.01 1000.36333,145.096667 991.07,141.27 L991.07,131.35 C1001.31,135.69 1014.37,137.86 1030.25,137.86 C1041.91667,137.86 1051.17,135.193333 1058.01,129.86 C1064.63211,124.937171 1068.45656,117.10935 1068.27,108.86 C1068.27,103.48 1067.13667,99.0233333 1064.87,95.49 C1062.60333,91.9566667 1058.91,88.7333333 1053.79,85.82 C1048.67,82.9066667 1041.15,79.7633333 1031.23,76.39 C1016.71,71.3966667 1006.68,66 1001.14,60.2 C995.6,54.4 992.83,46.67 992.83,37.01 C992.60872,26.8474142 997.248533,17.1888775 1005.32,11.01 C1013.64667,4.30333333 1024.35667,0.95 1037.45,0.95 C1050.381,0.863415128 1063.19098,3.44379508 1075.08,8.53 L1071.48,17.09 C1059.68,12.1633333 1048.4,9.7 1037.64,9.7 C1027.14,9.7 1018.77667,12.13 1012.55,16.99 C1006.32333,21.85 1003.21333,28.46 1003.22,36.82 C1003.22,42.0666667 1004.17667,46.3766667 1006.09,49.75 C1008.40388,53.5194867 1011.62973,56.6456037 1015.47,58.84 C1019.81,61.5066667 1027.26333,64.73 1037.83,68.51 C1048.91667,72.3366667 1057.25,76.0466667 1062.83,79.64 C1068.41,83.2333333 1072.47667,87.3166667 1075.03,91.89 C1077.61667,96.4233333 1078.91,101.93 1078.91,108.41 Z" id="logo-name-path-13"></path>
    <filter x="-107.0%" y="-21.9%" width="314.0%" height="228.7%" filterUnits="objectBoundingBox" id="logo-name-filter-14">
        <feOffset dx="0" dy="2" in="SourceAlpha" result="shadowOffsetOuter1"></feOffset>
        <feGaussianBlur stdDeviation="1" in="shadowOffsetOuter1" result="shadowBlurOuter1"></feGaussianBlur>
        <feColorMatrix values="0 0 0 0 0   0 0 0 0 0   0 0 0 0 0  0 0 0 0.2 0" type="matrix" in="shadowBlurOuter1" result="shadowMatrixOuter1"></feColorMatrix>
        <feOffset dx="0" dy="4" in="SourceAlpha" result="shadowOffsetOuter2"></feOffset>
        <feGaussianBlur stdDeviation="2" in="shadowOffsetOuter2" result="shadowBlurOuter2"></feGaussianBlur>
        <feColorMatrix values="0 0 0 0 0   0 0 0 0 0   0 0 0 0 0  0 0 0 0.2 0" type="matrix" in="shadowBlurOuter2" result="shadowMatrixOuter2"></feColorMatrix>
        <feOffset dx="0" dy="8" in="SourceAlpha" result="shadowOffsetOuter3"></feOffset>
        <feGaussianBlur stdDeviation="4" in="shadowOffsetOuter3" result="shadowBlurOuter3"></feGaussianBlur>
        <feColorMatrix values="0 0 0 0 0   0 0 0 0 0   0 0 0 0 0  0 0 0 0.2 0" type="matrix" in="shadowBlurOuter3" result="shadowMatrixOuter3"></feColorMatrix>
        <feOffset dx="0" dy="16" in="SourceAlpha" result="shadowOffsetOuter4"></feOffset>
        <feGaussianBlur stdDeviation="8" in="shadowOffsetOuter4" result="shadowBlurOuter4"></feGaussianBlur>
        <feColorMatrix values="0 0 0 0 0   0 0 0 0 0   0 0 0 0 0  0 0 0 0.2 0" type="matrix" in="shadowBlurOuter4" result="shadowMatrixOuter4"></feColorMatrix>
        <feOffset dx="0" dy="32" in="SourceAlpha" result="shadowOffsetOuter5"></feOffset>
        <feGaussianBlur stdDeviation="16" in="shadowOffsetOuter5" result="shadowBlurOuter5"></feGaussianBlur>
        <feColorMatrix values="0 0 0 0 0   0 0 0 0 0   0 0 0 0 0  0 0 0 0.1 0" type="matrix" in="shadowBlurOuter5" result="shadowMatrixOuter5"></feColorMatrix>
        <feMerge>
            <feMergeNode in="shadowMatrixOuter1"></feMergeNode>
            <feMergeNode in="shadowMatrixOuter2"></feMergeNode>
            <feMergeNode in="shadowMatrixOuter3"></feMergeNode>
            <feMergeNode in="shadowMatrixOuter4"></feMergeNode>
            <feMergeNode in="shadowMatrixOuter5"></feMergeNode>
        </feMerge>
    </filter>
</defs>
<g id="logo-name-Symbols" stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
    <g id="logo-name-Two-Legs" transform="translate(30.000000, 0.000000)" fill-rule="nonzero">
        <g id="logo-name-Group-2">
            <g id="logo-name-Shape">
                <use fill="black" fill-opacity="1" filter="url(#logo-name-filter-2)" xlinkHref="#logo-name-path-1"></use>
                <use fill="#FFFFFF" fill-rule="evenodd" xlinkHref="#logo-name-path-1"></use>
            </g>
            <g id="logo-name-Shape">
                <use fill="black" fill-opacity="1" filter="url(#logo-name-filter-4)" xlinkHref="#logo-name-path-3"></use>
                <use fill="#FFFFFF" fill-rule="evenodd" xlinkHref="#logo-name-path-3"></use>
            </g>
            <g id="logo-name-Shape">
                <use fill="black" fill-opacity="1" filter="url(#logo-name-filter-6)" xlinkHref="#logo-name-path-5"></use>
                <use fill="#FFFFFF" fill-rule="evenodd" xlinkHref="#logo-name-path-5"></use>
            </g>
            <g id="logo-name-Shape">
                <use fill="black" fill-opacity="1" filter="url(#logo-name-filter-8)" xlinkHref="#logo-name-path-7"></use>
                <use fill="#FFFFFF" fill-rule="evenodd" xlinkHref="#logo-name-path-7"></use>
            </g>
            <g id="logo-name-Shape">
                <use fill="black" fill-opacity="1" filter="url(#logo-name-filter-10)" xlinkHref="#logo-name-path-9"></use>
                <use fill="#FFFFFF" fill-rule="evenodd" xlinkHref="#logo-name-path-9"></use>
            </g>
            <g id="logo-name-Shape">
                <use fill="black" fill-opacity="1" filter="url(#logo-name-filter-12)" xlinkHref="#logo-name-path-11"></use>
                <use fill="#FFFFFF" fill-rule="evenodd" xlinkHref="#logo-name-path-11"></use>
            </g>
            <g id="logo-name-Shape">
                <use fill="black" fill-opacity="1" filter="url(#logo-name-filter-14)" xlinkHref="#logo-name-path-13"></use>
                <use fill="#FFFFFF" fill-rule="evenodd" xlinkHref="#logo-name-path-13"></use>
            </g>
        </g>
    </g>
</g>
</svg>  
  
    );
  }
}
